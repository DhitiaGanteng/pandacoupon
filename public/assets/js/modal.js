
function infoDialog(type, text, action){
    var checkType = { 
        default: 'type-default', info: 'type-info', primary: 'type-primary', 
        success: 'type-success', warning: 'type-warning', danger: 'type-danger'
    }   

    var config = {
        question: {
            type: checkType['warning'],
            icon: 'fa-question-circle',
            label: 'Warning !',
            cssClass: 'btn-warning'
        },
        info: {
            type: checkType['info'],
            icon: 'fa-info',
            label: 'Information',
            cssClass: 'btn-info'
        },
        error: {
            type: checkType['danger'],
            icon: 'fa-exclamation-circle',
            label: 'Oops, Error!',
            cssClass: 'btn-danger'
        },
        success:{
            type: checkType['success'],
            icon: 'fa-check',
            label: 'Congratz!',
            cssClass: 'btn-success'
        }
    }
    

    var dialog = BootstrapDialog.show({
        type: config[type].type,
        title: '<i class="fa '+config[type].icon+'"></i>  '+config[type].label+'',
        message: text,
        buttons: [{
            label: 'Ok',
            cssClass: config[type].cssClass,
            action: function(){
                    action();
                    dialog.close();
            }
        },
        {
            label: 'Close',
            action: function(){
                dialog.close();
            }
        }]
    }); 

    dialog.open();    
}

