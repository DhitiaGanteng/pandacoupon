const CouponModel = require('../models/coupon_m');
const StoreModel = require('../models/store_m');
const TagModel = require('../models/tag_m');
const dateFormat = require('dateformat');
const BreadcrumbsHelper = require('../helpers/breadcrumbs');

const set = {
    renderView: 'dashboard/coupons/coupon',
    renderForm: 'dashboard/coupons/coupon_form',
    trendingView: 'dashboard/coupons/trending',
    baseURL: 'dashboard/coupon',
    formURL : 'coupon/form/',
    redirectURL: '/dashboard/store/info/'
}

module.exports = {
    index(req, res, next){        
        const Breadcrumbs = new BreadcrumbsHelper('Coupon  Lists');
        const breadcrumbs = Breadcrumbs.add('Dashboard','/')
                            .add('Coupons','/dashboard/coupon');

        const stores = StoreModel.find({}).exec();
        stores.then((stores)=>{
            return res.render(set.renderView,{
                stores,
                breadcrumbs
            });
        });
        
    },
    form(req, res, next){
        const Breadcrumbs = new BreadcrumbsHelper('Coupon  Lists');
        const breadcrumbs = Breadcrumbs.add('Dashboard','/')
                            .add('Coupons','/dashboard/coupon')
                            .add('Coupon Form','#')

        const offerTypes = ['Online Code', 'In-Store Coupon','Online Sale or Tip'];
        const labelCoupon = ['Coupon', 'Sale', 'Deal', 'Promo'];

        const { url_name, coupon_id } = req.params;
        
        const tag = TagModel.find({}).exec();
        const coupon = CouponModel.findById(coupon_id).exec();
        Promise.all([tag, coupon])
            .then((result)=>{
               
               
            return res.render(set.renderForm,{
            
                props: req.flash('validProps'),
                errors: req.flash('errors'),
                success_msg: req.flash('success'),
                url_name,
                offerTypes,
                labelCoupon,
                tags: result[0],
                coupon: result[1],
                dateFormat,
                breadcrumbs
            });
        });
        
    },
    create(req, res, next){
      
        const { url_name, code, coupon_id } = req.body;
        if(!req.body.verified){
            req.body.verified = '';
        }
        req.body.offer = { type: req.body.offer_type, link: req.body.offer_link }
         if(req.body.offer_type !== 'Online Code'){
            req.check('offer_link', 'You must enter Offer Link').notEmpty();
         }
         req.check('title','Title is required.').notEmpty();
         req.check('code','Code is required.').notEmpty();
         req.check('description','Description is required.').notEmpty();
         req.check('discount','Discount is required.').notEmpty();
         req.check('labelCoupon', 'Label Coupon is required.').notEmpty();
         req.check('startDate','Start date is required.').notEmpty();
         req.check('expiredDate','Expired date is required.').notEmpty();
         req.check('tags','Tags is required.').notEmpty();
                       
         const errors = req.validationErrors();
                      
         if(errors){
            req.flash('errors', errors);

            if(coupon_id){
                return res.redirect('/dashboard/coupon/edit/'+coupon_id);
            }else{
                req.flash('validProps', req.body);
                return res.redirect(set.formURL+url_name);
            }
            
          }
        if(coupon_id){
            // updating
            CouponModel.findByIdAndUpdate(coupon_id, req.body, (err, result)=>{
                if(err) return next(err);

                req.flash('success', 'Succesfully updating the coupon');
                return res.redirect(set.redirectURL+url_name);
            });
            
        }else{
            // inserting
        const checkCoupon = CouponModel.findOne({ 'store.url': url_name, code }).exec();
        const stores = StoreModel.findOne({ url_name }).exec();
       
            Promise.all([checkCoupon, stores])
                .then((result)=>{
                    if(result[0] !== null){
                        req.flash('errors', { msg: 'Code Coupon '+code+' has been submitted in '+result[1].name+' database' });
                        return res.redirect(set.formURL+url_name);
                    }else{
                       
                        req.body.store =  { id: result[1]._id, name: result[1].name, url: result[1].url_name, photo: result[1].photo };
                        req.body.createdBy = req.user.id;
            
                        CouponModel.create(req.body)
                            .then(coupon=>{  
                                req.flash('success', 'A new coupon code is succesfully inserted');
                                return res.redirect(set.redirectURL+url_name);
                        });
                    }
                
                });    
        }
      
    },
    trendingCoupon(req, res, next){
        const Breadcrumbs = new BreadcrumbsHelper('Trending Coupon Lists');
        const breadcrumbs = Breadcrumbs.add('Dashboard','/')
                            .add('Coupon','/dashboard/coupon')
                            .add('Trending','/dashboard/coupon/view/trending');
                            
        CouponModel.find({ 'trending': true },'title store.name store.url expiredStatus', (err, coupons)=>{
            return res.render(set.trendingView,{
                coupons,
                breadcrumbs
            });
        });
        
    },
    trendingUpdate(req, res, next){
        const { id, trending, url_name } = req.body;
        CouponModel.findByIdAndUpdate(id, req.body, (err, result)=>{
            return res.redirect(set.redirectURL+url_name);
        });
        
    },
    updateTrending(req, res, next){
        const { id, trending } = req.query;
        
        CouponModel.findByIdAndUpdate(id, { trending }, (err, result)=>{
            if(err) return next(err);
            
            return res.send({ success: true });
        })
        
        
      
    },
   
}