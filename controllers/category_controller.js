const CategoryModel = require('../models/category_m');
const BreadcrumbsHelper = require('../helpers/breadcrumbs');

module.exports = {

    index(req, res, next){
        const Breadcrumbs = new BreadcrumbsHelper('Category View');
        const breadcrumbs = Breadcrumbs.add('Dashboard','/').add('Category','/dashboard/category');
        const props = {
            errors: req.flash('errors'),
            success_msg: req.flash('success'),
            cat: '',
            breadcrumbs
        }
        
        CategoryModel.find({})
            .then(data=>{
                //tricky
                props.data = data;
                const { id } = req.params;
                if(id){
                    CategoryModel.findById(id, (err, cat)=>{
                        if(err) return next(err);
                        if(cat){
                            props.cat = cat;
                            res.render('dashboard/categories/category', props);
                        }
                    });
                }else{
                    res.render('dashboard/categories/category', props);
                }
                
            })
            .catch(next);
        
    },
    create(req, res, next){
        const categoryProps = req.body;
        if(req.body.id){
            CategoryModel.findByIdAndUpdate(req.body.id, categoryProps, (err, result)=>{
                if(err){
                    req.flash('errors', 'Updating failed.');
                    return res.redirect('/dashboard/category');    
                }

                req.flash('success','Successfully update Category');
                return res.redirect('/dashboard/category');
            });

        }else{
            CategoryModel.findOne({ catName: req.body.catName }, (err, cat)=>{
                if(err) return next(err);
                if(cat){
                    req.flash('errors', ''+req.body.catName+' is on database.');
                    return res.redirect('/dashboard/category');
                }
                CategoryModel.create(categoryProps)
                    .then(cat=>{
                        req.flash('success',''+req.body.catName+' is Successfully Inserted.')
                        return res.redirect('/dashboard/category');
                    })
                    .catch((err)=>{
                        req.flash('errors', err.errors.catName.message);
                        return res.redirect('/dashboard/category');
                
                    });
            });
        }
            
    },
    destroy(req, res, next){
        CategoryModel.findByIdAndRemove(req.params.id, (err)=>{
            if(err) return next(err);
            
            req.flash('success', 'Successfully delete a category');
            return res.redirect('/dashboard/category'); 
        })
    }
}