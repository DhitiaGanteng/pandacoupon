const AdminModel = require('../models/admin_m');
const passportService = require('../services/passport');

const BreadcrumbsHelper = require('../helpers/breadcrumbs');


module.exports = {
    index(req, res, next){
        const { id } = req.params;
        
        const Breadcrumbs = new BreadcrumbsHelper('Admin View');
        const breadcrumbs = Breadcrumbs.add('Dashboard','/').add('Admin','/dashboard/admin');
    
        const admin =  AdminModel.findById(id).exec();
        const admins = AdminModel.find({});
        Promise.all([admins, admin])
            .then(result=>{
                return res.render('dashboard/admin/admin',{
                    errors: req.flash('errors'),
                    success_msg: req.flash('success'),
                    validValue: req.flash('validValue'),
                    role: ['Helper', 'Admin'],
                    admins: result[0],
                    admin: result[1],
                    breadcrumbs
                })
            })
          
        
    },
    signin(req, res, next){

      const { email, password } = req.body;

      if(!email || !password ){
        req.flash('loginMessage', "Email & Password is required.");
        res.redirect('/');
      }else{
          next();
      }
    },
    signup(req, res, next){
        const adminProps = req.body;
        req.checkBody('email', 'Email is required.').notEmpty();
        req.checkBody('password', 'Password is required').notEmpty();
        req.checkBody('confirmpassword', 'Confirm Password is required.').notEmpty();
        req.checkBody('confirmpassword', 'Confirm Password is not match.').equals(req.body.password);

        const errors = req.validationErrors();
       
        if(errors){
             req.flash('validValue', req.body);
            req.flash('errors', errors);
            return res.redirect('/dashboard/admin')
        }else{

            AdminModel.findOne({ email: req.body.email}, (err, admin)=>{
                if(err) return next(err);
                if(admin){
                    req.flash('validValue', req.body);
                    req.flash('errors',{ msg: 'Email in Use.' });
    
                    return res.redirect('/dashboard/admin')
                }
                AdminModel.create(adminProps)
                    .then(admin=>{
                        req.flash('success', 'Successfully adding new '+req.body.role+' ');
                    
                        return res.redirect('/dashboard/admin');
                    })
                    .catch(next);
            });


        }
    },
    update(req, res, next){
        
        AdminModel.findByIdAndUpdate(req.body.id, req.body, (err, result)=>{
            req.flash('success', 'Successfully updating data');
            return res.redirect('/dashboard/admin');
        });
    }
}
