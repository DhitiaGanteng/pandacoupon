const StoreModel = require('../models/store_m');
const CategoryModel = require('../models/category_m');
const CouponModel = require('../models/coupon_m');
const config = require('../config');
const cloudinary = require('cloudinary');
const multiparty = require('multiparty');
const BreadcrumbsHelper = require('../helpers/breadcrumbs');

const set = {
    url:{
        redirect: '/dashboard/store',
        redirectRecommended: '/dashboard/store/view/recommended',
        redirectError: '/error/404'
    },
    view:{
        recommended: 'dashboard/stores/recommended'
    }
}

module.exports = {
    index: function(req, res, next){
       const Breadcrumbs = new BreadcrumbsHelper('Stores View');
       const breadcrumbs = Breadcrumbs.add('Dashboard','/').add('Stores','/dashboard/store');
       const category =  CategoryModel.find({}).exec();
       const store = StoreModel.find({}).exec();
       const { id } = req.params;
       const storeById = StoreModel.findById(id).exec();
       
       Promise.all([category, store, storeById])
        .then((result)=>{
            
            return res.render('dashboard/stores/store', {
                errors: req.flash('errors'),
                success_msg: req.flash('success'),
                category: result[0],
                stores: result[1],
                store: result[2],
                breadcrumbs
            });
        })
        .catch(err=>{
            return res.redirect(set.redirectError);
        });
    },
    create(req, res, next){
        
        const form = new multiparty.Form();
        form.parse(req, (err, fields, files)=>{
            // set config cloudinary
            cloudinary.config(config.cloudinary);
            req.body = fields;
            console.log(fields);
            const id = fields.id[0];
          
           if(id){
                //Update 
               const upload = new Promise((resolve, reject)=>{
                   if(files.photo[0].originalFilename){
                   cloudinary.uploader.upload(files.photo[0].path, function(result) {

                        return resolve(result);

                    },{ public_id: fields.public_id[0], invalidate: true });
                   }else{
                       return resolve();
                   }
               });

               upload.then((result)=>{
                   if(result){
                      fields.photo = result.url;
                      fields.public_id = result.public_id;
                   }
                    StoreModel.findByIdAndUpdate(id, fields, (err, result)=>{
                        if(err){
                            req.flash('errors', 'Updating failed.');
                            return res.redirect(set.url.redirect);
                        }

                        req.flash('success', 'successfully update store');
                        return res.redirect(set.url.redirect);
                    });    
               })
            
           
              
           }else{
               //Create
            StoreModel.findOne({ name: fields.name }, (err, result)=>{
                if(err){
                    req.flash('errors', { msg: 'Failed to add a new store' });
                    return res.redirect(set.url.redirect);
                }
                if(result){
                    req.flash('errors', { msg: ''+fields.name[0]+' is on database.'});
                    return res.redirect(set.url.redirect);
                }else{

                    req.check('name','Store name is required.').notEmpty();
                    req.check('description', 'Store Description is required.').notEmpty();
                    req.check('category', 'Store Category is required.').notEmpty();
                    
                    const errors = req.validationErrors();
                    console.log(errors);
                    if(errors){
                        req.flash('errors', errors);
                        return res.redirect(set.url.redirect);
                    }
                    
                    //upload file to cloudinary
                    var upload = new Promise((resolve, reject)=>{
                        if(files.photo[0].originalFilename){
                            cloudinary.uploader.upload(files.photo[0].path, (result)=>{
                                return resolve(result); 
                            });
                        }else{
                            return resolve({ url: '', public_id: ''});
                        }
                            
                      });
                    
                    upload.then(result=>{
                        
                        fields.photo = result.url;
                        fields.public_id = result.public_id;

                        StoreModel.create(fields)
                            .then(result=>{
                                req.flash('success', ''+fields.name[0]+' Store is successfully inserted.');
                                return res.redirect(set.url.redirect);
                            });
                    })            
                }

            });
           }            
        });
        
    },
    info(req, res, next){
        
        const { url_name, page } = req.params;
        const perPage= 10;
      
        const Breadcrumbs = new BreadcrumbsHelper('Store Information');
        const breadcrumbs = Breadcrumbs.add('Dashboard','/')
                            .add('Store','/dashboard/store')
                            .add('Information','/dashboard/store/info/'+url_name);

      
        const stores = StoreModel.findOne({ url_name }).populate({
                path: 'category',
                model: 'category'
              }).exec();
              
        const coupons = CouponModel.find({ 'store.url': url_name }).skip( perPage * page ).limit( perPage ).sort('-created_at').exec();
        const countCoupon = CouponModel.find({ 'store.url': url_name }).count().exec();

        Promise.all([stores, coupons, countCoupon])
            .then((result)=>{
               if(!result[0]){
                   return res.redirect('/error/404');
               }

                return res.render('dashboard/stores/store_info',{
                    success_msg: req.flash('success'),
                    url_name,
                    page,
                    store: result[0],
                    coupons: result[1],
                    pages: result[2] / perPage,
                    breadcrumbs
                });
        })
        .catch((err)=>{
           return res.redirect(set.redirectError);
        })
      

       
    },
    recommended(req, res, next){
        const Breadcrumbs = new BreadcrumbsHelper('Recommended Store Lists');
        const breadcrumbs = Breadcrumbs.add('Dashboard','/')
                            .add('Stores','/dashboard/store')
                            .add('Recommended','/dashboard/store/view/recommended');

        const { id, recommended } = req.body;
         
        if(id){
           
            const val = (recommended === 'true' ? false : true);
            StoreModel.findByIdAndUpdate(id, { 'recommended': val }, (err, result)=>{
                if(err) return next(err);
                return res.redirect('/dashboard/store/view/recommended');
            });
        }else{
           
            StoreModel.find({},'name recommended url_name', (err, stores)=>{
            
                return res.render(set.view.recommended,{
                    stores,
                    breadcrumbs
                });
            });
        }
        
    }
}