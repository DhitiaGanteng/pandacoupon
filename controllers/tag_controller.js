const TagModel = require('../models/tag_m');
const BreadcrumbsHelper = require('../helpers/breadcrumbs');


module.exports = {
    index(req, res, next){  
        const Breadcrumbs = new BreadcrumbsHelper('Tags View');
        const breadcrumbs = Breadcrumbs.add('Dashboard','/').add('Tag','/dashboard/tag');
        const { id } = req.params;
        const tags = TagModel.find({}).exec();
        const tag = TagModel.findById(id).exec();
       
        Promise.all([tags, tag])
            .then((result)=>{
    
                return res.render('dashboard/tags/tag',{
                    errors: req.flash('errors'),
                    success_msg: req.flash('success'),
                    tags: result[0],
                    tag: result[1],
                    breadcrumbs
                });
             });
        
    },
    create(req, res, next){
        const { id, tagName } = req.body;
        if(id){
            TagModel.findByIdAndUpdate(id, req.body, (err, result)=>{
                if(err){
                    req.flash('errors', 'Updating failed.');
                    return res.redirect('/dashboard/tag');    
                }

                req.flash('success','Successfully update Category');
                return res.redirect('/dashboard/tag');
            });
        }else{
            TagModel.findOne({ tagName }, (err, tag)=>{
                if(err) return next(err);
                if(tag){
                    req.flash('errors', ''+tagName+' is on database.');
                    return res.redirect('/dashboard/tag');
                }

                 TagModel.create(req.body)
                    .then((err, result)=>{
                        req.flash('success',''+tagName+' is Successfully Inserted.')
                        return res.redirect('/dashboard/tag');
                    })
                    .catch((err)=>{
                        req.flash('errors', err.errors.tagName.message);
                        return res.redirect('/dashboard/tag');
                    })
            })
           
        }
    }
}