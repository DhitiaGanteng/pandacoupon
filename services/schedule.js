var cronJob = require('cron').CronJob;
const CouponModel = require('../models/coupon_m');

var job = new cronJob({
  cronTime: '00 00 24 * * 1-7',
  onTick: function() {
  // Runs everyday
  // at exactly 12:00:00 AM.
 var now = new Date();
 CouponModel.updateMany({ expiredDate: { "$lte": now } }, { expiredStatus: true } )
            .then((err,result)=>{
                console.log('Coupons have been updated!')
            })
            .catch(err=>{
               console.log(err);
            })
  },
  start: false,
  timeZone: "Asia/Jakarta"
});


job.start();

  //cronTime: '15 * * * * *',