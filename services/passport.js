const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const AdminModel = require('../models/admin_m');

passport.serializeUser((user, done)=>{
    done(null, user._id);
});

passport.deserializeUser((id, done)=>{
    AdminModel.findById(id, (err, user)=>{
        done(err, user);
    });
});

passport.use('local-login', new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true
}, (req, email, password, done)=>{

    AdminModel.findOne({ email }, (err, user)=>{
        if(err) return done(err);
        if(!user){
            return done(null, false, req.flash('loginMessage', 'No Admin has been found.'));
        }

        if(!user.comparePassword(password)){
            return done(null, false, req.flash('loginMessage', 'Oops! Wrong Password Or Email'))
        }

        if(user.status == false){
            return done(null, false, req.flash('loginMessage', 'Oops! User is not active.'))
        }

        return done(null, user);

    });
}));

const requireAuth = passport.authenticate('local-login',{
    successRedirect: '/dashboard',
    failureRedirect: '/',
    failureFlash: true
});


const isAuthenticated = (req, res, next)=>{
    if(req.isAuthenticated()){
        return next();
    }else{
        req.flash('loginMessage', 'You are not authorized.');
        res.redirect('/');
    }
}

module.exports = {
    requireAuth,
    isAuthenticated
}
