const router = require('express').Router();
const passportService = require('../services/passport');
const AdminController = require('../controllers/admin_controller');


router.get('/', (req, res, next)=>{
    if(req.user){
        return res.redirect('/dashboard');
    }else{
        res.render('login',{
            message: req.flash('loginMessage')
        });
    }
    
});

router.get('/error/404', (req, res, next)=>{
    res.render('errors/404');
})



router.get('/logout', (req, res, next)=>{
    req.logout();
    res.render('logout');
});


router.post('/signin', [AdminController.signin,passportService.requireAuth]);

module.exports = router;
