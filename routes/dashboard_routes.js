const router = require('express').Router();
const passportService = require('../services/passport');
const AdminController = require('../controllers/admin_controller');
const CategoryController = require('../controllers/category_controller');
const StoreController = require('../controllers/store_controller');
const CouponController = require('../controllers/coupon_controller');
const TagController = require('../controllers/tag_controller');
const SubscriberModel = require('../models/subscriber_m');
const BreadcrumbsHelper = require('../helpers/breadcrumbs');


router.get('/', passportService.isAuthenticated,(req, res, next)=>{

    const test = new BreadcrumbsHelper('Dashboard');
    const breadcrumbs = test.add('Dashboard', '/')
                                           
    res.render('dashboard/dashboard', {
        breadcrumbs
    });
});

router.get('/subscriber', (req, res, next)=>{
    const Breadcrumbs = new BreadcrumbsHelper('Subscriber View');
    const breadcrumbs = Breadcrumbs.add('Dashboard','/').add('Subscriber','/dashboard/subscriber');
    SubscriberModel.find({}).sort('-created_at')
        .then(subscribers=>{
            res.render('dashboard/subscriber/subscriber',{
                subscribers,
                breadcrumbs
            });
        })
    
});

router.get('/admin', AdminController.index);
router.get('/admin/:id', AdminController.index);
router.get('/admin/signup', (req, res, next)=>{
   res.send({ error: req.flash('errors')})
});

router.post('/admin/signup', AdminController.signup);
router.post('/admin/update', AdminController.update);
router.get('/coupon', CouponController.index);
router.get('/coupon/form/:url_name', CouponController.form);
router.get('/coupon/edit/:coupon_id', CouponController.form);
router.get('/coupon/view/trending', CouponController.trendingCoupon);
router.get('/coupon/view/trending/update', CouponController.updateTrending)
router.post('/coupon', CouponController.create);
router.post('/coupon/trending', CouponController.trendingCoupon);
router.post('/coupon/update/?:trending', CouponController.trendingUpdate);


router.get('/store', StoreController.index);
router.get('/store/:id', StoreController.index);
router.get('/store/info/:url_name', StoreController.info);
router.get('/store/info/:url_name/page/:page', StoreController.info);
router.get('/store/view/recommended', StoreController.recommended);

router.post('/store', StoreController.create);
router.post('/store/recommended', StoreController.recommended);


router.get('/category', CategoryController.index);
router.get('/category/:id', CategoryController.index);
router.post('/category', CategoryController.create);
router.get('/category/destroy/:id', CategoryController.destroy);

router.get('/tag', TagController.index);
router.get('/tag/:id', TagController.index);
router.post('/tag', TagController.create);

router.get('error/404', (req, res, next)=>{
    res.render('errors/404');
});

module.exports = router;