const express = require('express');
const app = express();
const mongoose = require('mongoose');
const passport = require('passport');
const morgan = require('morgan');
const ejs = require('ejs');
const ejsMate = require('ejs-mate');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const expressValidator = require('express-validator');
const expressSession = require('express-session');
const mongoStore = require('connect-mongo')(expressSession);
const flash = require('express-flash');

const config = require('./config');
const mainRoutes = require('./routes');
const dashboardRoutes = require('./routes/dashboard_routes');
const passportService = require('./services/passport');
const Schedule = require('./services/schedule');

mongoose.Promise = global.Promise;
if(process.env.NODE_ENV !== 'test'){
    mongoose.connect(config.dbURI);
}

app.set('port', process.env.PORT || "1313");
app.use(express.static('public'));
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(expressValidator({
  errorFormatter: function(param, msg, value) {
      var namespace = param.split('.')
      , root    = namespace.shift()
      , formParam = root;

    while(namespace.length) {
      formParam += '[' + namespace.shift() + ']';
    }
    return {
      param : formParam,
      msg   : msg,
      value : value
    };
  }
}));
app.engine('ejs', ejsMate);
app.set('view engine', 'ejs');
app.use(expressSession({
    secret: config.sessionSecret,
    saveUninitialized: false,
    resave: false,
    store: new mongoStore({ url: config.dbURI, autoReconnect: true })
}));
app.use(passport.initialize());
app.use(passport.session());
app.use((req, res, next)=>{
    res.locals.user = req.user;
    next();
});
app.use(flash());

app.all("/dashboard/*",passportService.isAuthenticated, function(req, res, next){
  next();
})
app.use('/', mainRoutes);
app.use('/dashboard', dashboardRoutes);

module.exports = app;
