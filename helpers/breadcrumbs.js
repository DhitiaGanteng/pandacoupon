const Breadcrumbs = function(title){

    this.title = title;
    this.breadLists = '';

}

Breadcrumbs.prototype.add = function(title, url){
        var breadArray = [];

        breadArray.push("<li><a href='"+url+"'>"+title+"</a></li>");

        breadArray.map(val=>{
            this.breadLists += val;
        });

       return this;
}



module.exports = Breadcrumbs;