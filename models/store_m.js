const mongoose = require('mongoose');
const URLSlugs = require('mongoose-url-slugs');

const Schema = mongoose.Schema;

const StoreSchema = new Schema({
    name: { type: String, required: [true, 'Store Name is required.'] },
    description: { type: String, default: ''},
    link:{
        website: String,
        affiliate: String,
    },
    photo: { type: String, default: ''},
    category: { type: Schema.Types.ObjectId, ref:'category'},
    offers: { type: Number, default: 0 },
    recommended: { type: Boolean, default: false },
    public_id: String
}, { timestamps: { createdAt: 'created_at' } });

StoreSchema.index({ url_name: 1, name: 1 }, {unique: true});

StoreSchema.plugin(URLSlugs('name', { field: 'url_name'}));
StoreSchema.post('save', function(next){
    const Category = mongoose.model('category');
    Category.findByIdAndUpdate(this.category,{ $inc: {stores: 1}}, ()=> next);
});

const Shop = mongoose.model('store', StoreSchema);

module.exports = Shop;


