const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TagSchema = new Schema({ 
    tagName: { type: String, required:[true, 'Tag Name is required.']}
}, { timestamps: { createdAt: 'created_at' } });

const Tag = mongoose.model('tag', TagSchema);

module.exports = Tag;