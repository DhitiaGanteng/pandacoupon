const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const SubscriberSchema = new Schema({
    email: { type: String, unique: true, required: true },
    geo: {
        range: [],
        country: String,
        region: String,
        city: String,
        ll: [],
        metro: String
    }

},{ collection: 'subscribers'})

const Subscriber = mongoose.model('subscriber', SubscriberSchema);

module.exports = Subscriber;