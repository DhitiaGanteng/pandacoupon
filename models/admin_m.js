const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt-nodejs');

const AdminSchema = new Schema({
    email: { type: String, unique: true, lowercase: true },
    password: { type: String, default: ''},
    fullname: String,
    role: String,
    status: { type: Boolean, default: true }
}, { timestamps: { createdAt: 'created_at' } });

AdminSchema.pre('save', function(next){
    const admin = this;
    if(!admin.isModified('password')) return next();

    bcrypt.genSalt(10, function(err, salt){
        if(err) return next(err);
        bcrypt.hash(admin.password, salt, null, function(err, hash){
            if(err) next(err);
            admin.password = hash;
            next();
        });
    });
});

AdminSchema.methods.comparePassword = function(candidatePassword){
    return bcrypt.compareSync(candidatePassword, this.password);
}

const Admin = mongoose.model('admin', AdminSchema);

module.exports = Admin;

