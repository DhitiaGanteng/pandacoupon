const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CouponSchema = new Schema({
    title: { type: String, required: true},
    offer:{
        type: { type: String, default: ''},
        link: { type: String, default: ''}
    },
    code: { type: String, required: true},
    description: {type: String, required: true},
    startDate: { type: Date, default: Date.now},
    expiredDate: { type: Date, default: Date.now },
    expiredStatus: { type: Boolean, default: false},
    category: { type: String, default: 'Sale'},
    tags: [{ type: String, default: '' }],
    labelCoupon: { type: String, default: '' },
    discount: {type: String, required: true},
    createdBy: { type: Schema.Types.ObjectId, ref: 'admin'},
    store: {    
        id: { type: Schema.Types.ObjectId },
        name: String, 
        url: String,
        photo: String,
        website: String,
        affiliateLink: String
     },
    used: { type: Number, default: 0 },
    verified: { type: String, default: '' },
    trending: { type: Boolean, default: false }
}, { timestamps: { createdAt: 'created_at' } });

CouponSchema.index({expiredDate: 1});

CouponSchema.pre('save', function(next){
    const minimum = 100;
    const maximum = 1000;
    var randomnumber = Math.floor(Math.random() * (maximum - minimum + 1)) + minimum;
    this.used = randomnumber;
    next();
});

CouponSchema.post('save', function(next){
    const Store = mongoose.model('store');
    Store.findByIdAndUpdate(this.store.id,{ $inc: {offers: 1}}, ()=> next);
});

const Coupon = mongoose.model('coupon',CouponSchema);

module.exports = Coupon;