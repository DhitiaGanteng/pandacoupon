const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CategorySchema = new Schema({ 
    catName: { type: String, required:[true, 'Category Name is required.']},
    stores: { type: Number, default: 0 },
    icon: { type: String, default: 'fa fa-random'}
}, { timestamps: { createdAt: 'created_at' } });

const Category = mongoose.model('category', CategorySchema);

module.exports = Category;